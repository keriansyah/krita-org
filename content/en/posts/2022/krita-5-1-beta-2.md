---
title: "Krita 5.1 Beta 2"
date: "2022-07-15"
categories: 
  - "development-builds"
  - "news"
---

Today we're releasing the second beta for Krita 5.1! For the full list, check out the work-in-progress full [release notes](https://krita.org/en/krita-5-1-release-notes/)! Or check out Wojtek Trybus release video!

{{< youtube TnvCjziCUGI >}}

 
{{< support-krita-callout >}}

Warning: beta 1 had a bug where brush presets were saved incorrectly, leading to crashes when trying to load those presets in other versions of Krita. [Here is a Python script](https://invent.kde.org/tymond/brushes-metadata-fixer) that can fix those presets.

Since [the first beta](https://krita.org/en/item/first-beta-for-krita-5-1-0-released/), the following bugs have been fixed:

- Fix changing workspaces selecting different palettes. [BUG:446634](https://bugs.kde.org/show_bug.cgi?id=446634)
- Fix loading templates if the resource folder is in a non standard location. [BUG:452706](https://bugs.kde.org/show_bug.cgi?id=452706)
- Fix occasional crash selecting a preset when a tag is removed. [BUG:454052](https://bugs.kde.org/show_bug.cgi?id=454052)
- Clear the name of a workspace after saving it. [BUG:446652](https://bugs.kde.org/show_bug.cgi?id=446652)
- Fix cycling updates in the color smudge brush configuration page. [BUG:455244](https://bugs.kde.org/show_bug.cgi?id=455244)
- Make it possible to save and load resources with dots in the name to bundles. [BUG:453702](https://bugs.kde.org/show_bug.cgi?id=453702)
- Make it possible to use the G'Mic curves filter more than once. [BUG:455891](https://bugs.kde.org/show_bug.cgi?id=455891)
- Fix an assert when trying to export a palette. [BUG:454949](https://bugs.kde.org/show_bug.cgi?id=454949)
- Fix crash when saving an image with text to PSD. [BUG:455988](https://bugs.kde.org/show_bug.cgi?id=455988)
- Fix infinite recursions in the layer color label selector. [BUG:456201](https://bugs.kde.org/show_bug.cgi?id=456201)
- Set the size of the selection's marching ants based on the screen's dpi. [BUG:456364](https://bugs.kde.org/show_bug.cgi?id=456364)
- Reduce idle cpu usage when a selection is active in an image that is not currently shown. [BUG:432284](https://bugs.kde.org/show_bug.cgi?id=432282)
- Fix crash when closing the settings dialog window under some circumstances. [BUG:445329](https://bugs.kde.org/show_bug.cgi?id=445329)
- Fix issues with Grid settings becoming invalid when cropping. [BUG:447588](https://bugs.kde.org/show_bug.cgi?id=447588)
- Fix random artefacts in the image projection when undoing brush strokes. [BUG:453243](https://bugs.kde.org/show_bug.cgi?id=453243)
- Increase the maximum pattern scaling to 10000%.
- Fix an assert when a grayscale file layer fails to load the image. [BUG:456201](https://bugs.kde.org/show_bug.cgi?id=456201)
- Fix layer order when using multiple layer inputs for G'Mic filters. [BUG:456463](https://bugs.kde.org/show_bug.cgi?id=456463)
- Fix drag and drop of shape layers when the source and destination images have a different resolution. [BUG:456450](https://bugs.kde.org/show_bug.cgi?id=456450)
- Fix a crash when trying to remove multiple layers at once.
- Fix a crash when dragging and dropping a vector layer from one image to another. [BUG:456450](https://bugs.kde.org/show_bug.cgi?id=456450)
- Fix an assert in on document saving that caused freezes on autosaving. [BUG:456404](https://bugs.kde.org/show_bug.cgi?id=456404)
- Make the enclose and fill tool take into account image limits. [BUG:456299](https://bugs.kde.org/show_bug.cgi?id=456299)
- Fix rotating rectangles.
- Fix dropping reference images not working at all. [BUG:456382](https://bugs.kde.org/show_bug.cgi?id=4563822)
- Improve the performance of the brush outline cursor. [BUG:456080](https://bugs.kde.org/show_bug.cgi?id=456080)
- Fix the layer name for layers added through drag and drop. [BUG:455671](https://bugs.kde.org/show_bug.cgi?id=455671)
- Fix positioning of pasted images. [BUG:453247](https://bugs.kde.org/show_bug.cgi?id=453247)
- Fix fast mode when blending mode, opacity or eraser is used in the fill tool.
- Fix a crash when trying to filter an active selection. [BUG:455844](https://bugs.kde.org/show_bug.cgi?id=455844)
- Improve undo of opacity animation keyframe changes. [BUG:454547](https://bugs.kde.org/show_bug.cgi?id=454547)
- Fix a crash when setting an opacity keyframe on a copied layer. [BUG:454547](https://bugs.kde.org/show_bug.cgi?id=454547)
- Fix an assert when moving an animated transform mask
- Fix brush outlines of predefined brushes. [BUG:455912](https://bugs.kde.org/show_bug.cgi?id=455912)
- Update the meshgradient properly if the stop color is changed.
- Fix editing the stops of a vector shape gradient. [BUG:455794](https://bugs.kde.org/show_bug.cgi?id=455794), [BUG:447464](https://bugs.kde.org/show_bug.cgi?id=447464), [BUG:449606](https://bugs.kde.org/show_bug.cgi?id=449606)
- Fix issues when exporting an animation to JPEG-XL. [BUG:455597](https://bugs.kde.org/show_bug.cgi?id=455597), [BUG:455598](https://bugs.kde.org/show_bug.cgi?id=455598)
- Support YCbCr and JPEG compression in TIFF files.
- Make it possible to load krita files with 1ppi resolution. [BUG:444291](https://bugs.kde.org/show_bug.cgi?id=444291)
- Improve the refresh rate of the overview docker. [BUG:443674](https://bugs.kde.org/show_bug.cgi?id=443674)
- Android: use the correct full-screen icon in the statusbar. [BUG:456065](https://bugs.kde.org/show_bug.cgi?id=456065)
- Python: set the image modified when changing an annotation. [BUG:441704](https://bugs.kde.org/show_bug.cgi?id=441704)
- Fix copy/pasting of reference images. [BUG:454515](https://bugs.kde.org/show_bug.cgi?id=454515)
- Fix crash on undo after importing an assistant set. [BUG:455584](https://bugs.kde.org/show_bug.cgi?id=455584)
- Fix slowdowns when using OpenColorIO.
- Fix visibility for Vanishing Point Assistant and Ellipse Assistant.
- Python: make Node::setColorSpace return false if the specified profile couln't be retrieved. [BUG:454812](https://bugs.kde.org/show_bug.cgi?id=454812)
- Fix a crash after assigning "one finger drag" to zoom and rotate canvas. [BUG:455241](https://bugs.kde.org/show_bug.cgi?id=455241)
- Fix a crash when opening G'Mic when the image is busy.
- Optimzie all svg icons.

## Download

### Windows

If you're using the portable zip files, just open the zip file in Explorer and drag the folder somewhere convenient, then double-click on the krita icon in the folder. This will not impact an installed version of Krita, though it will share your settings and custom resources with your regular installed version of Krita. For reporting crashes, also get the debug symbols folder.

Note that we are not making 32 bits Windows builds anymore.

- 64 bits Windows Installer: [krita-x64-5.1.0-beta2-setup.exe](https://download.kde.org/unstable/krita/5.1.0-beta2/krita-x64-5.1.0-beta2-setup.exe)
- Portable 64 bits Windows: [krita-x64-5.1.0-beta2.zip](https://download.kde.org/unstable/krita/5.1.0-beta2/krita-x64-5.1.0-beta2.zip)
- [Debug symbols. (Unpack in the Krita installation folder)](https://download.kde.org/unstable/krita/5.1.0-beta2/krita-x64-5.1.0-beta2-dbg.zip)

### Linux

- 64 bits Linux: [krita-5.1.0-beta2-x86\_64.appimage](https://download.kde.org/unstable/krita/5.1.0-beta2/krita-5.1.0-beta2-x86_64.appimage)

The separate gmic-qt appimage is no longer needed.

(If, for some reason, Firefox thinks it needs to load this as text: to download, right-click on the link.)

### macOS

Note: if you use macOS Sierra or High Sierra, please [check this video](https://www.youtube.com/watch?v=3py0kgq95Hk) to learn how to enable starting developer-signed binaries, instead of just Apple Store binaries.

- macOS disk image: [krita-5.1.0-beta2.dmg](https://download.kde.org/unstable/krita/5.1.0-beta2/krita-5.1.0-beta2.dmg)

### Android

The Android releases are made from the release tarball, so there are translations. We consider Krita on ChromeOS and Android still **_beta_**.

- [64 bits Intel CPU APK](https://download.kde.org/unstable/krita/5.1.0-beta2/krita-x86_64-5.1.0-beta2-release-signed.apk)
- [32 bits Intel CPU APK](https://download.kde.org/unstable/krita/5.1.0-beta2/krita-x86-5.1.0-beta2-release-signed.apk)
- [64 bits Arm CPU APK](https://download.kde.org/unstable/krita/5.1.0-beta2/krita-arm64-v8a-5.1.0-beta2-release-signed.apk)
- [32 bits Arm CPU APK](https://download.kde.org/unstable/krita/5.1.0-beta2/krita-armeabi-v7a-5.1.0-beta2-release-signed.apk)

### Source code

- [krita-5.1.0-beta2.tar.gz](https://download.kde.org/unstable/krita/5.1.0-beta2/krita-5.1.0-beta2.tar.gz)
- [krita-5.1.0-beta2.tar.xz](https://download.kde.org/unstable/krita/5.1.0-beta2/krita-5.1.0-beta2.tar.xz)
