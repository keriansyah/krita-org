<?php
/*
 * Make sure to disable the display of errors in production code!
 */
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

require_once __DIR__ . ".\\vendor\\autoload.php";
require_once __DIR__ . ".\\functions.php";


/*
 * Initialize the Mollie API library with your API key.
 * See: https://www.mollie.com/dashboard/developers/api-keys
 */
$mollie = new \Mollie\Api\MollieApiClient();
$mollie->setApiKey("MOLLIEAPIKEY");


// need to convert the amount to a string like "10.00"
$payment_amount = (string)$_POST["donation_amount"] . ".00";

$payment = $mollie->payments->create([
    "amount" => [
        "currency" => "EUR",
        "value" => $payment_amount
    ],
    "description" => "Krita Foundation One-time donation",
    "redirectUrl" => calculate_redirect_url(),
]);



/*
* Send the customer off to complete the payment.
* This request should always be a GET, thus we enforce 303 http response code
*/
header("Location: " . $payment->getCheckoutUrl(), true, 303);



function calculate_redirect_url() {
    $protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === 0 ? 'https://' : 'http://';
    $full_url = $protocol . "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $base_url = explode("php", $full_url)[0]; // get base domain name part of URL without the php folder

    // add language code (2 character code only supported right now)
    $language_code = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);;
    $redirect_url = $base_url;
    
    // append the language code if not english. since english is default
    // language, hugo does not use it when generating URLs
    if($language_code != "en") {
        $redirect_url = $redirect_url . $language_code;
    }
    return $redirect_url . "one-time-donation-thank-you/";
}


?>

